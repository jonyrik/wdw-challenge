<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package acme
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<nav class="navbar navbar-expand-md">
    <div class="container">
        <div class="row">
            <div class="col">
                <a class="logo-top" href="#">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/logo.svg" alt="" style="opacity: 0;">
                </a>
            </div>
            <div class="col">
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </div>
    </div>
</nav>
<nav class="navbar navbar-expand-md fixed-top">
    <div class="container">
        <div class="row">
            <div class="col">
                <a class="logo-top" href="<?php bloginfo('url'); ?>">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/logo.svg" alt="">
                </a>
            </div>
            <div class="col">
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                    <div class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">Login</div>
                </div>
            </div>
        </div>
    </div>
</nav>
