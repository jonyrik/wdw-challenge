<?php
/**
 Template Name: Home
 */

get_header();

?>

<header>
    <div class="container">
        <div class="row">
            <div class="col">
                <h1><?php the_title(); ?></h1>
            </div>
        </div>
    </div>
</header>

<main class="container-fluid">
    <div class="row header-search">
        <div class="col col-desktop"></div>
        <div class="col">
            <div class="row">
                <div class="col search-label">
                    Search by #
                </div>
                <div class="col">
                    <input type="text" id="search" onkeyup="myFunction()" placeholder="">
                </div>
            </div>
        </div>
    </div>

    <ul id="gallerylist" class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
        <?php
            $args = array(
                'post_type' => 'gallery',
                'order' => 'ASC',
                'role' => 'gallery_author',
            );

            $gallery_loop = new WP_Query( $args );

            if ($gallery_loop->have_posts()) :
                while ($gallery_loop->have_posts()) : $gallery_loop->the_post();
                    get_template_part( 'template-parts/content', 'image' );
                endwhile;
            endif;
            ?>
    </ul>

    <div class="row">
        <a href="#" class="col-12" id="loadMore">Load More</a>
    </div>

</main><!-- /.container -->

<!-- modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <form class="form-signin">
                    <h1 class="h3 mb-3 fw-normal">Please Login</h1>
                    <label for="inputEmail" class="visually-hidden">Email address</label>
                    <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
                    <label for="inputPassword" class="visually-hidden">Password</label>
                    <input type="password" id="inputPassword" class="form-control" placeholder="Password" required>
                    <div class="checkbox mb-3">
                        <label>
                            <input type="checkbox" value="remember-me"> Remember me
                        </label>
                    </div>
                    <button class="btn btn-lg btn-primary" type="submit">Send</button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>
