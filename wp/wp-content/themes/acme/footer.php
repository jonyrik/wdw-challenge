<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package acme
 */

?>

<footer>
    <div class="container text-center">
        <div class="in-footer">
            <div class="logo-footer">
                <img src="<?php echo get_template_directory_uri() ?>/assets/img/logo-footer.svg" alt="">
            </div>
            <div class="copyright">© All right reserved 2020</div>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
