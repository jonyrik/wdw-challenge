<li class="col">
    <div class="card">
        <div class="img" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>');"></div>
        <div class="card-body">
            <div class="row">
                <div class="author-img" style="background-image: url('<?php echo get_avatar_url(get_the_author_meta( 'ID' )); ?>')"></div>

                <div class="col">
                    <div class="author">
                        <?php the_author(); ?>
                    </div>
                    <div class="hash">
                        <?php
                        $terms = get_the_terms($post->ID, 'hashtag');
                        foreach ($terms as $term) {
                            echo $term->name;
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</li>