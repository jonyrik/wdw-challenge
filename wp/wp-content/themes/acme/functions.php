<?php
/**
 * acme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package acme
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'acme_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function acme_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on acme, use a find and replace
		 * to change 'acme' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'acme', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'acme' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'acme_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'acme_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function acme_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'acme_content_width', 640 );
}
add_action( 'after_setup_theme', 'acme_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function acme_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'acme' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'acme' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'acme_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function acme_scripts() {
	wp_enqueue_style( 'acme-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_enqueue_style( 'acme-styles', get_template_directory_uri() . '/assets/css/styles.min.css', array(), _S_VERSION );
//	wp_style_add_data( 'acme-style', 'rtl', 'replace' );

	wp_enqueue_script( 'acme-jquery', '//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'acme-bootstrap', '//cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'acme-loadmore', get_template_directory_uri() . '/assets/js/loadmore.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'acme-search', get_template_directory_uri() . '/assets/js/search.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'acme_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

function cptui_register_my_cpts() {

    /**
     * Post Type: Galería de Imagenes.
     */

    $labels = [
        "name" => __( "Galería de Imagenes", "acme" ),
        "singular_name" => __( "Galería de Imagenes", "acme" ),
        "menu_name" => __( "Gallery Image", "acme" ),
        "all_items" => __( "Todas las Imágenes", "acme" ),
        "add_new" => __( "Añadir nuevo", "acme" ),
        "add_new_item" => __( "Añadir nueva Imagen", "acme" ),
        "edit_item" => __( "Editar Imagen", "acme" ),
        "new_item" => __( "Nueva Imagen", "acme" ),
        "view_item" => __( "Ver Imagen", "acme" ),
        "view_items" => __( "Ver Imagenes", "acme" ),
        "search_items" => __( "Buscar Imagen", "acme" ),
        "not_found" => __( "No se ha encontrado la Imagen", "acme" ),
        "not_found_in_trash" => __( "No se han encontrado la Imagen en la papelera", "acme" ),
        "parent" => __( "Imagen superior", "acme" ),
        "featured_image" => __( "Imagen destacada para Gallería de Imágenes", "acme" ),
        "set_featured_image" => __( "Establece una imagen destacada para Gallería de Imágenes", "acme" ),
        "remove_featured_image" => __( "Eliminar la imagen destacada de Gallería de Imágenes", "acme" ),
        "use_featured_image" => __( "Usar como imagen destacada de Gallería de Imágenes", "acme" ),
        "archives" => __( "Archivos de Gallería de Imágenes", "acme" ),
        "insert_into_item" => __( "Insertar en Gallería de Imágenes", "acme" ),
        "uploaded_to_this_item" => __( "Subir a Gallería de Imágenes", "acme" ),
        "filter_items_list" => __( "Filtrar la lista de Gallería de Imágenes", "acme" ),
        "items_list_navigation" => __( "Navegación de la lista de Gallería de Imágenes", "acme" ),
        "items_list" => __( "Lista de Gallería de Imágenes", "acme" ),
        "attributes" => __( "Atributos de Gallería de Imágenes", "acme" ),
        "name_admin_bar" => __( "Image Gallery", "acme" ),
        "item_published" => __( "Imagen publicada", "acme" ),
        "item_published_privately" => __( "Imagen publicada como privada.", "acme" ),
        "item_reverted_to_draft" => __( "Imagen devuelta a borrador.", "acme" ),
        "item_scheduled" => __( "Imagea programada", "acme" ),
        "item_updated" => __( "Imagen actualizada.", "acme" ),
        "parent_item_colon" => __( "Imagen superior", "acme" ),
    ];

    $args = [
        "label" => __( "Galería de Imagenes", "acme" ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => true,
        "rest_base" => "",
        "rest_controller_class" => "WP_REST_Posts_Controller",
        "has_archive" => false,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "delete_with_user" => false,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => [ "slug" => "gallery", "with_front" => true ],
        "query_var" => true,
        "menu_position" => 1,
        "menu_icon" => "dashicons-format-gallery",
        "supports" => [ "title", "thumbnail", "author" ],
        "taxonomies" => [ "hashtag" ],
    ];

    register_post_type( "gallery", $args );
}

add_action( 'init', 'cptui_register_my_cpts' );


function cptui_register_my_taxes() {

    /**
     * Taxonomy: Hashtags.
     */

    $labels = [
        "name" => __( "Hashtags", "acme" ),
        "singular_name" => __( "Hashtag", "acme" ),
        "menu_name" => __( "Hashtags", "acme" ),
        "all_items" => __( "Todos los Hashtags", "acme" ),
        "edit_item" => __( "Editar Hashtag", "acme" ),
        "view_item" => __( "Ver Hashtag", "acme" ),
        "update_item" => __( "Actualizar el nombre de Hashtag", "acme" ),
        "add_new_item" => __( "Añadir nuevo Hashtag", "acme" ),
        "new_item_name" => __( "Nombre del nuevo Hashtag", "acme" ),
        "parent_item" => __( "Hashtag superior", "acme" ),
        "parent_item_colon" => __( "Hashtag superior", "acme" ),
        "search_items" => __( "Buscar Hashtags", "acme" ),
        "popular_items" => __( "Hashtags populares", "acme" ),
        "separate_items_with_commas" => __( "Separar Hashtags con comas", "acme" ),
        "add_or_remove_items" => __( "Añadir o eliminar Hashtags", "acme" ),
        "choose_from_most_used" => __( "Escoger entre los Hashtags más usandos", "acme" ),
        "not_found" => __( "No se ha encontrado Hashtags", "acme" ),
        "no_terms" => __( "Ningún Hashtags", "acme" ),
        "items_list_navigation" => __( "Navegación de la lista de Hashtags", "acme" ),
        "items_list" => __( "Lista de Hashtags", "acme" ),
        "back_to_items" => __( "Volver a Hashtags", "acme" ),
    ];

    $args = [
        "label" => __( "Hashtags", "acme" ),
        "labels" => $labels,
        "public" => true,
        "publicly_queryable" => true,
        "hierarchical" => false,
        "show_ui" => true,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "query_var" => true,
        "rewrite" => [ 'slug' => 'hashtag', 'with_front' => true, ],
        "show_admin_column" => false,
        "show_in_rest" => true,
        "rest_base" => "hashtag",
        "rest_controller_class" => "WP_REST_Terms_Controller",
        "show_in_quick_edit" => false,
    ];
    register_taxonomy( "hashtag", [ "gallery" ], $args );
}
add_action( 'init', 'cptui_register_my_taxes' );

