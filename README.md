**ACME Gallery**
============
Para levantar el proyecto correctamente:

+ **Instalar Yarn**

`yarn install`

-------

## **Para editar**

+ **Compilar estilos**

Crea un archivo _styles.css_ en la carpeta _assets/css_

`yarn sass`


+ **Minificar hoja de estilos**

Crea un archivo _styles.min.css_ en la carpeta _assets/css_

`yarn prod`

-------

Puede visualizar el sitio online en el siguiente [Link](https://estudiofrenesi.com.ar/clientes/wdw/)

-------

## **Wordpress**

Puede visualizar el sitio online en Wordpress en el siguiente [Link](https://estudiofrenesi.com.ar/clientes/wdw/challenge/)

Puede ingresar al Dashboard del Wordpress desde el siguiente [Link](https://estudiofrenesi.com.ar/clientes/wdw/challenge/wp-admin/)

Las credenciales se encuentran en el archivo credenciales.txt

La DB es wpacme.sql

La carpeta del proyecto es "wp"