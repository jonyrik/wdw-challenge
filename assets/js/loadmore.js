/*
	Load more content with jQuery - May 21, 2013
	(c) 2013 @ElmahdiMahmoud
*/

$(function () {
    $("li").slice(0, 6).show().css( "display", "list-item");
    $("#loadMore").on('click', function (e) {
        e.preventDefault();
        $("li:hidden").slice(0, 6).slideDown();
        if ($("li:hidden").length == 0) {
            $("#load").fadeOut('slow');
        }
        $('html,body').animate({
            scrollTop: $(this).offset().top
        }, 1500);
    });
});